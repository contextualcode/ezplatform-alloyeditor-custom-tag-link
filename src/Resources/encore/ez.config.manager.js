const path = require('path');
const eZRichTextPath = '../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/';

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-css',
        newItems: [
            path.resolve(__dirname, '../public/scss/custom_tag_link.scss'),
        ]
    });

    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        itemToReplace: path.resolve(__dirname, eZRichTextPath + 'js/OnlineEditor/buttons/ez-btn-customtag-update.js'),
        newItem: path.resolve(__dirname, '../public/js/OnlineEditor/buttons/ez-btn-customtag-update.js'),
    });
};
